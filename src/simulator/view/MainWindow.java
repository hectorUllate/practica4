package simulator.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import simulator.control.Controller;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
	
	Controller _ctrl;
	
	public MainWindow(Controller ctrl){
		super("Physics Simulator");
		_ctrl = ctrl;
		initGUI();
	}
	
	private void initGUI(){
		JPanel mainPanel = new JPanel(new BorderLayout());
		
		this.setBounds(400, 100, 900, 700); //TODO CAMBIAR ESTO; SOLO DE PRUEBA
		this.setResizable(false);//No se si esto hay que quitarlo
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // TODO implementar dialogo de confirmacion antes de cerrar
		this.setLocationRelativeTo(null); // TODO esto es para que salga centrado en la pantalla
		
		this.addWindowListener(new WindowListener() {
			//Clase WindowAdapter() : clase con todos los metodos implementados vacios.
			@Override
			public void windowActivated(WindowEvent arg0) {
			}
			@Override
			public void windowClosed(WindowEvent arg0) {
			}
			@Override
			public void windowClosing(WindowEvent arg0) {
				if (JOptionPane.showConfirmDialog(null, "Are you sure that you want to close the program?", 
						"Exit", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					System.exit(0);
			}
			@Override
			public void windowDeactivated(WindowEvent arg0) {
			}
			@Override
			public void windowDeiconified(WindowEvent arg0) {
			}
			@Override
			public void windowIconified(WindowEvent arg0) {
			}
			@Override
			public void windowOpened(WindowEvent arg0) {
			}
		});
		
		mainPanel.add(new ControlPanel(_ctrl), BorderLayout.PAGE_START);
		mainPanel.add(new StatusBar(_ctrl), BorderLayout.PAGE_END);
		
		JPanel subPanel = new JPanel();
		subPanel.setLayout(new BoxLayout(subPanel, BoxLayout.Y_AXIS));
		
		BodiesTable bt = new BodiesTable(_ctrl);
		bt.setPreferredSize(new Dimension(900, 200));
		bt.setMinimumSize(new Dimension(900, 200));
		bt.setMaximumSize(new Dimension(900, 200));
		subPanel.add(bt);
		Viewer v = new Viewer(_ctrl);
		v.setPreferredSize(new Dimension(900, 390));
		v.setMinimumSize(new Dimension(900, 390));
		v.setMaximumSize(new Dimension(900, 390));
		subPanel.add(v);

		mainPanel.add(subPanel, BorderLayout.CENTER);
		
		this.setIconImage(new ImageIcon("resources/icons/mainIcon.png").getImage());
		
		this.setContentPane(mainPanel);
		this.setVisible(true);
	}
}
