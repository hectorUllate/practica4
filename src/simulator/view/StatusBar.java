package simulator.view;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import simulator.control.Controller;
import simulator.model.Body;
import simulator.model.SimulatorObserver;

@SuppressWarnings("serial")
public class StatusBar extends JPanel implements SimulatorObserver {
	
	// ...
	// TODO falta un JLabel para cada value de los cuerpos que será la JLabel que irá cambiando segun lo que modifiquen los observadores
	private JLabel _currTime;
	private JLabel _currLaws;
	private JLabel _numOfBodies;
	private JToolBar _statusBar;
	private String defaultLaw;
	
	StatusBar(Controller ctrl) {
		initGUI();
		this.defaultLaw = ctrl.getGravityLawsFactory().getInfo().get(0).getString("desc");
		ctrl.addObserver(this);
	}

	private void initGUI() {
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.setBorder(BorderFactory.createBevelBorder(1));
		this.setSize(new Dimension(900, 100));
		
		_statusBar = new JToolBar();
		_currTime = new JLabel("Time:  0.0");
		_numOfBodies = new JLabel("Bodies:  0");
		_currLaws = new JLabel("Laws:  " + defaultLaw);
		/*
		_currTime.setText("0.0");
		_numOfBodies.setText("0");
		// TODO mirar porque si el usuario introduce por la linea de argumentos un file de entrada, debe aparecer el que ponga en el file
		_currLaws.setText(defaultLaw); // Por defecto aparece esa, luego cambiará si escoges otra
		*/
		_statusBar.add(_currTime);
		_statusBar.addSeparator();
		_statusBar.add(_numOfBodies);
		_statusBar.addSeparator();
		_statusBar.add(_currLaws);
		
		this.add(_statusBar);
		this.setVisible(true);
	}

	// SimulatorObserver methods
	@Override
	public void onRegister(List<Body> bodies, double time, double dt, String gLawDesc) {
		_currTime.setText("Time:  " + time);
		_numOfBodies.setText("Bodies:  0");
		_currLaws.setText("Laws:  " + gLawDesc);
	}

	@Override
	public void onReset(List<Body> bodies, double time, double dt, String gLawDesc) {
		_currTime.setText("Time:  0.0");
		_numOfBodies.setText("Bodies:  0");
		_currLaws.setText("Laws:  " + defaultLaw);
	}

	@Override
	public void onBodyAdded(List<Body> bodies, Body b) {
		_numOfBodies.setText("Bodies:  " + bodies.size());
	}

	@Override
	public void onAdvance(List<Body> bodies, double time) {
		_currTime.setText("Time:  " + time);
	}

	@Override
	public void onDeltaTimeChanged(double dt) {
	}

	@Override
	public void onGravityLawChanged(String gLawDesc) {
		_currLaws.setText("Laws:  " + gLawDesc);
	}

}