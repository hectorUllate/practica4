package simulator.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import simulator.control.Controller;
import simulator.misc.Vector;
import simulator.model.Body;
import simulator.model.SimulatorObserver;

@SuppressWarnings("serial")
public class Viewer extends JPanel implements SimulatorObserver {

	private int _centerX;
	private int _centerY;
	private double _scale;
	private List<Body> _bodies;
	private boolean _showHelp;
	
	Viewer(Controller ctrl){
		initGUI();
		ctrl.addObserver(this);
	}
	
	private void initGUI(){
		//TODO add border with title
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black, 2), "Viewer",
				TitledBorder.LEFT, TitledBorder.TOP));
		
		
		_bodies = new ArrayList<>();
		_scale = 1.0;
		_showHelp = true;
		
		this.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				
				switch(e.getKeyChar()){
					case '-':
						_scale = _scale * 1.1;
						break;
					case '+':
						_scale = Math.max(1000.0, _scale / 1.1);
						break;
					case '=':
						autoScale();
						break;
					case 'h':
						_showHelp = !_showHelp;
						break;
				}
				repaint();
			}
		});
		
		/* TODO si tenemos que cambiar el Layout en ejecucion, debemos ejecutar un metodo que se llama this.revalidate()
		 * 
		 * "revalidate() is to be called when u change an anttribute that would affect their width/height and call reapaint()
		 * when u change an attribute that would affect theis appearance"
		 * */
		
		/* TODO Si vamos a implementar pocas funciones del MouseListener(), podemos utilizar la clase MouseAdapter()
		 * La ventaja de esta clase es que ya tiene todas las implementaciones vacias, entonces solo tenemos que hacer
		 * @Override a la funcion que queramos implementar.
		 * */
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setFocusable(false);
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				setFocusable(true);
				requestFocus();
			}
		});
	}
	
	@Override
	protected void paintComponent(Graphics g){
		
		super.paintComponent(g);
		Graphics2D gr = (Graphics2D) g;
		gr.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		gr.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		//Use 'gr' to draw, NOT 'g'
		
		//calculate the center
		_centerX = getWidth() / 2;
		_centerY = getHeight() / 2;
		
		//TODO draw a cross at center
		gr.drawLine(_centerX - 5, _centerY, _centerX + 5, _centerY); //Linea horizontal
		gr.drawLine(_centerX, _centerY - 5, _centerX, _centerY + 5); //Linea vertical
		//TODO draw bodies
		gr.setColor(Color.blue);
		for(Body b : _bodies){
			gr.fillOval(_centerX + (int) (b.getPosition().coordinate(0)/_scale), 
					_centerY - (int) (b.getPosition().coordinate(1)/_scale), 5, 5);
			gr.drawString(b.getId(), _centerX + (int) (b.getPosition().coordinate(0)/_scale) + 7, 
					_centerY - (int) (b.getPosition().coordinate(1)/_scale) - 7);
		}
		//TODO draw help if _showHelp is true
		if(_showHelp){
			gr.setColor(Color.red);
			gr.drawString("h: toggle help, +: zoom-in, -: zoom-out, =:fit", 10, 25);
			gr.setColor(Color.red);
			gr.drawString("Scaling ratio: " + _scale, 10, 38);
		}
			

	}
	
	private void autoScale() {
		double max = 1.0;
		
		for(Body b : _bodies){
			Vector p = b.getPosition();
			for(int i = 0; i < p.dim(); ++i)
				max = Math.max(max, Math.abs(b.getPosition().coordinate(i)));
		}
		
		double size = Math.max(1.0, Math.min((double) getWidth(), (double) getHeight()));
		
		_scale = max > size ? 4.0 * max / size : 1.0;
	}
	
	
	@Override
	public void onRegister(List<Body> bodies, double time, double dt, String gLawDesc) {
		autoScale();
		repaint();
	}

	@Override
	public void onReset(List<Body> bodies, double time, double dt, String gLawDesc) {
		_bodies.clear();
		autoScale();
		repaint();
	}

	@Override
	public void onBodyAdded(List<Body> bodies, Body b) {
		_bodies.add(b);
		autoScale();
		repaint();
	}

	@Override
	public void onAdvance(List<Body> bodies, double time) {
		repaint();
	}

	@Override
	public void onDeltaTimeChanged(double dt) {
	}

	@Override
	public void onGravityLawChanged(String gLawDesc) {
	}

}