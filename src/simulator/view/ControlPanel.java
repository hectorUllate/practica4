package simulator.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.json.JSONObject;

import simulator.control.Controller;
import simulator.factories.Factory;
import simulator.model.Body;
import simulator.model.GravityLaws;
import simulator.model.SimulatorObserver;

@SuppressWarnings("serial")
public class ControlPanel extends JPanel implements SimulatorObserver {
	
	// ...
	private Controller _ctrl;
	private boolean _stopped;
	private JToolBar _toolBar;
	private JButton _open, _laws, _run, _stop, _exit;
	private JSpinner _spinner;
	private JTextField _dt;
	private boolean allowPlay;
	
	ControlPanel(Controller ctrl) {
		this._ctrl = ctrl;
		this._stopped = true;
		this._open = new JButton();
		this._laws = new JButton();
		this._run = new JButton();
		this._stop = new JButton();
		this._exit = new JButton();
		this._dt = new JTextField();
		this._spinner = new JSpinner(new SpinnerNumberModel(10000, 500, 1000000, 500));
		this._toolBar = new JToolBar();
		this.allowPlay = false;
		
		initGUI();
		
		this._ctrl.addObserver(this);
	}
	
	private void run_sim(int n) {
		if(n > 0 && !_stopped) {
			try {
				_ctrl.run(1);
			} catch (Exception e) {
				showDialogException(e.getMessage()); // TODO show the error in a dialog box
				enableOrDisableComponents(true); // TODO enable all buttons
				_stopped = true;
				return;
			}
			
			SwingUtilities.invokeLater( new Runnable() {
				@Override
				public void run() {
					run_sim(n - 1);
				}
			});
		}
		else {
			_stopped = true;
			enableOrDisableComponents(true); // TODO enable all buttons
		}
	}
	
	// Activa o desactiva los botones segun el parametro que le pases. TRUE --> Activas. FALSE --> Desactivas
	private void enableOrDisableComponents(boolean enableOrDisable) {
		_open.setEnabled(enableOrDisable);
		_laws.setEnabled(enableOrDisable);
		_run.setEnabled(enableOrDisable);
		_exit.setEnabled(enableOrDisable);
		_spinner.setEnabled(enableOrDisable);
		_dt.setEnabled(enableOrDisable);
	}
	
	private void showDialogException(String msg) {
		String title = "Exception. Cause:";
		JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE, null);
	}
	
	private void addButton(JButton b, String toolTip, String imagePath) {
		//b.setBounds(0, 0, 30, 30);
		b.setToolTipText(toolTip);
		b.setIcon(new ImageIcon(imagePath));
		b.setVisible(true);
	}
	
	private void addStopButtonAction() {
		_stop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_stopped = true;
			}
		});
	}
	
	private void addExitButtonAction() {
		_exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "Are you sure that you want to close the program?", 
						"Exit", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					System.exit(0);
			}
		});
	}
	
	private void addPhysicsButtonAction() {
		_laws.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Factory<GravityLaws> laws = _ctrl.getGravityLawsFactory();
				/* Aqui tenemos las leyes que vamos a usar > Cogemos la info de la Factoria de leyes que es un ArrayList de JSON >
				   > cogemos la posicion 0, 1 y 2 y cogemos la key desc que es lo que mostraremos en la lista de desplegables */
				String[] choices = new String[laws.getInfo().size()];
				int pos = 0;
				for(JSONObject l: laws.getInfo()){
					choices[pos] = l.getString("desc");
					++pos;
				}
				String msg = "Select gravity law to be used.";
				String title = "Gravity Laws Selector";
				String selected = (String)JOptionPane.showInputDialog(null, msg, title, JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]) ;
				if(selected != null)
					_ctrl.setGravityLaws(laws.getInfo().get(getIndexOfGravityLaws(selected, laws)));
			}
		});
	}
	
	private int getIndexOfGravityLaws(String selected, Factory<GravityLaws> laws) {
		
		for (int i = 0; i < laws.getInfo().size(); i++) {
			if (laws.getInfo().get(i).getString("desc").equalsIgnoreCase(selected))
				return i;
		}
		/* TODO En ningún caso devolverá -1 pues en la lista desplegable tendrás que seleccionar una opcion si o si */
		return -1;
	}
	
	private void addRunButtonAction() {
		_run.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(allowPlay){
					enableOrDisableComponents(false);
					_stopped = false;
					_ctrl.setDeltaTime(Double.parseDouble(_dt.getText()));
					run_sim((Integer)_spinner.getValue());
				}
			}
		});
	}
	
	private void addOpenButtonAction() {
		_open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser _chooser = new JFileChooser();
				_chooser.setFileFilter(new FileNameExtensionFilter(".txt", "txt"));
				_chooser.setDialogTitle("Choose an input file");
				_chooser.setCurrentDirectory(new File("resources/"));
				
				if(_chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
					//Solo intentamos abrir el archivo si el usuario pulsa ACEPTAR
					_ctrl.reset();
					
					FileInputStream inStream = null;
					try {
						inStream = new FileInputStream(_chooser.getSelectedFile());
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					_ctrl.loadBodies(inStream);
					allowPlay = true;
					_run.setEnabled(allowPlay);
				}
			}
		});
	}
	
	private void initGUI() {
//		try{
//            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//       }catch(Exception ex){
//        	System.err.println("ERROR");
//       }
		
		this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		this.setSize(900, 100);
		_toolBar.addSeparator();
		
		addButton(_open, "Open selected input file", "resources/icons/open.png");
		_toolBar.add(_open);
		_toolBar.addSeparator();
		
		addButton(_laws, "Select gravity law to be used", "resources/icons/physics.png");
		_toolBar.add(_laws);
		_toolBar.addSeparator();
		
		addButton(_run, "Runs the simulation", "resources/icons/run.png");
		_toolBar.add(_run);
		
		addButton(_stop, "Stops the simulation", "resources/icons/stop.png");
		_toolBar.add(_stop);
		
		_toolBar.add(Box.createRigidArea(new Dimension(15, 40))); //Incluimos un area en blanco que actua de separador
		
		_toolBar.add(new JLabel("Steps: "));
		_toolBar.add(_spinner);
		_spinner.setPreferredSize(new Dimension(90, 40));
		_spinner.setMinimumSize(new Dimension(90, 40));
		_spinner.setMaximumSize(new Dimension(90, 40));
		_spinner.setToolTipText("Sets the number of steps");

		_toolBar.add(Box.createRigidArea(new Dimension(15, 40))); //Incluimos un area en blanco que actua de separador
		
		_toolBar.add(new JLabel("Delta - Time: "));
		_toolBar.add(_dt);
		_dt.setPreferredSize(new Dimension(80, 40));
		_dt.setMinimumSize(new Dimension(80, 40));
		_dt.setMaximumSize(new Dimension(80, 40));
		_dt.setToolTipText("Increases the time in delta-time");
		
		_toolBar.add(Box.createHorizontalGlue());
		
		_toolBar.addSeparator();
		addButton(_exit, "Exit and close the program", "resources/icons/exit.png");
		_toolBar.add(_exit);
		
		addOpenButtonAction();
		addPhysicsButtonAction();
		addStopButtonAction();
		addExitButtonAction();
		addRunButtonAction();
		
		if (_ctrl.is_inFile()) {
			_run.setEnabled(true);
			allowPlay = true;
		}
		
		if (!allowPlay)
			_run.setEnabled(false);
		
		this.add(this._toolBar);
		this.setVisible(true);
	}
	
	// SimulatorObserver methods
	@Override
	public void onRegister(List<Body> bodies, double time, double dt, String gLawDesc) {
		this._dt.setText(String.valueOf(dt));
		//if(!bodies.isEmpty()) this.allowPlay = true;
	}

	@Override
	public void onReset(List<Body> bodies, double time, double dt, String gLawDesc) {
		// Significa que se ha reseteado el simulador y debemos poner todo a los valores inciales como si no hubiera nada
		_dt.setText(String.valueOf(dt));
		_spinner.setValue(10000);
	}

	@Override
	public void onBodyAdded(List<Body> bodies, Body b) {
		this.allowPlay = true;
	}

	@Override
	public void onAdvance(List<Body> bodies, double time) {
	}

	@Override
	public void onDeltaTimeChanged(double dt) {
		// TODO es necesario comprobar aqui que dt sea > 0.0 ??? no, lo cambia el controlador y el se encarga de comprobar.
		/* Si el dt que "tiene" el observador es diferente que el que está en el cuadrito del delta-time, significa que ha cambiado y debemos
		 * actualizar su valor */
		//if (dt != Integer.parseInt(_dt.getText()))
		_dt.setText(String.valueOf(dt));
	}

	@Override
	public void onGravityLawChanged(String gLawDesc) {
	}
	
}