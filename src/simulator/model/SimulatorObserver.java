package simulator.model;

import java.util.List;

public interface SimulatorObserver {
	
	/* 
	 
	 * Estos métodos dan información sobre el significado de los eventos que notifican:
	 * bodies		==> lista de cuerpos actual
	 * dt			==> tiempo real por paso del simulador
	 * time			==> tiempo actual del simulador
	 * b			==> es un cuerpo
	 * gLawsDesc	==> string que describe las leyes de la gravedad (invocando al metodo toString() de cada ley)
	 
	 */
	
	public void onRegister(List<Body> bodies, double time, double dt, String gLawDesc);
	public void onReset(List<Body> bodies, double time, double dt, String gLawDesc);
	public void onBodyAdded(List<Body> bodies, Body b);
	public void onAdvance(List<Body> bodies, double time);
	public void onDeltaTimeChanged(double dt);
	public void onGravityLawChanged(String gLawDesc);
	
}