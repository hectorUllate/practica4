package simulator.model;

import java.util.ArrayList;
import java.util.List;

public class PhysicsSimulator {
	
	private double currentTime; 
	private double stepRealTime; // aka delta time. Si el valor no es valido --> IllegalArgumentException
	private GravityLaws currentGravityLaw; // Si el valor es NULL --> IllegalArgumentException
	private List<Body> bodies;
	private List<SimulatorObserver> observers;
	
	public PhysicsSimulator(GravityLaws laws, double time) {
		this.currentTime = 0;
		if(time < 0)
			throw new IllegalArgumentException("El tiempo real por paso no puede ser negativo.");
		this.stepRealTime = time;
		if(laws == null)
			throw new IllegalArgumentException("Se debe especificar la ley de la gravedad a utilizar en la simulación.");
		this.currentGravityLaw = laws;
		this.bodies = new ArrayList<Body>();
		this.observers = new ArrayList<SimulatorObserver>();
	}
	
	public void advance(){
		this.currentGravityLaw.apply(bodies);
		
		for(Body b: bodies)
			b.move(this.stepRealTime);
		
		this.currentTime += this.stepRealTime;
		
		// Enviamos una notificacion onAdvance A TODOS los observadores
		for (SimulatorObserver o : this.observers)
			o.onAdvance(this.bodies, this.currentTime);
	}
	
	public void addBody(Body b) throws IllegalArgumentException {
		
		if(this.bodies.contains(b))
			throw new IllegalArgumentException("El cuerpo con id " + b.getId() + " no ha sido añadido pues ya existía.");
		
		this.bodies.add(b);
		
		// Enviamos una notificacion onBodyAdded A TODOS los observadores
		for (SimulatorObserver o : this.observers)
			o.onBodyAdded(this.bodies, b);
	}
	
	@Override
	public String toString() {
		
		StringBuilder str = new StringBuilder("");
		str.append("{ \"time\": " + this.currentTime + ", \"bodies\": [");
		
		for (int i = 0; i < bodies.size(); i++) {
			str.append(this.bodies.get(i).toString());
			
			if (i != bodies.size()-1)
				str.append(", ");
		}
		
		str.append("] }");
		
		return str.toString();
	}
	
	// Vacía la lista de cuerpos y pone el tiempo a 0.0
	public void reset() {
		this.bodies.clear();
		this.currentTime = 0;
		//this.stepRealTime = 0; //TODO charli juraria que esto no hay que ponerlo a 0
		
		// Enviamos una notificacion onReset A TODOS los observadores
		for (SimulatorObserver o : this.observers)
			o.onReset(this.bodies, this.currentTime, this.stepRealTime, this.currentGravityLaw.toString());
	}
	
	// Cambia el "delta-time" y si no es válido lanza una excepción
	public void setDeltaTime(double dt) {
		
		if (dt <= 0.0)
			throw new IllegalArgumentException("Valor no válido para el delta-time.");
		
		this.stepRealTime = dt;
		
		// Enviamos una notificacion OnDeltaTime A TODOS los observadores
		for (SimulatorObserver o : this.observers)
			o.onDeltaTimeChanged(stepRealTime);
	}
	
	// Cambia las leyes de la gravedad
	public void setGravityLaws(GravityLaws gravityLaws) {
		
		if (gravityLaws == null)
			throw new IllegalArgumentException("La ley de la gravedad escogida no es válida.");
		
		this.currentGravityLaw = gravityLaws;
		
		// Enviamos una notificacion onGravityLawChanged A TODOS los observadores
		for (SimulatorObserver o : this.observers)
			o.onGravityLawChanged(this.currentGravityLaw.toString());
	}
	
	// Añade un observador a la lista de observadores
	public void addObserver(SimulatorObserver o) {
		
		if(!this.observers.contains(o)){
			this.observers.add(o);
			/*
			Enviar notificacion onRegister SOLO al observador que se acabade registrar
			para pasarle el estado actual del simulador.
			*/
			o.onRegister(this.bodies, this.currentTime, this.stepRealTime, this.currentGravityLaw.toString());
		}
		
	}

}