package simulator.model;

import java.util.List;

public class NoGravity implements GravityLaws {

	@Override
	// Este método de esta clase no hace nada, los cuerpos se mueven con una aceleración fija
	public void apply(List<Body> bodies) {}
	
	@Override
	public String toString() {
		return "No gravity (ng)";
	}

}