package simulator.model;

import simulator.misc.Vector;

public class MassLosingBody extends Body {
	
	private double lossFactor;
	private double lossFrequency;
	private double accumulatedTime;

	public MassLosingBody(String id, Vector position, Vector velocity, double mass,
			double lossFrequency, double lossFactor) {
		super(id, position, velocity, mass);
		
		this.lossFactor = lossFactor;
		this.lossFrequency = lossFrequency;
		this.accumulatedTime = 0;
	}
	
	@Override
	void move(double t) {
		
		super.move(t);
		
		if(this.accumulatedTime < this.lossFrequency) this.accumulatedTime+=t;
		else{
			super.applyReduction(lossFactor);
			this.accumulatedTime = 0;
		}	
	}	
}