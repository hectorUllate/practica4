package simulator.model;

import java.util.List;

import simulator.misc.Vector;

public class NewtonUniversalGravitation implements GravityLaws {
	
	private static final double G_CTE = 6.67E-11;

	@Override // Calcula todas las fuerzas que aplican los cuerpos Bn sobre cada uno de ellos
	public void apply(List<Body> bodies) {
		
		// Inicializamos el vector a la misma dimensión que el resto de vectores de la lista "bodies"
		Vector Fi;
		for (Body a :bodies) {
			// Si el cuerpo tiene masa, aplicamos la fórmula de la fuerza y cambiamos la aceleración
			if (a.getMass() > 0.0) {
				Fi = new Vector(a.getAcceleration().dim());
				for (Body b: bodies) {
					if (!a.getId().equals(b.getId())) {
						// Vamos sumando en Fi para luego cambiar la aceleracion del cuerpo (i) con respecto del resto
						Fi = Fi.plus(getFij(a,b));
					}
				}
				// a = Fi * (1 / mi)
				a.setAcceleration(Fi.scale(1 / a.getMass()));
			}
			// Si no tiene masa, los vectores de aceleración y velocidad se ponen a 0.0
			else {
				a.setAcceleration(a.getAcceleration().scale(0));
				a.setVelocity(a.getVelocity().scale(0));
			}
		}
	}

	private Vector getFij(Body a, Body b){
		double fij = 0.0;
		double dist = 0.0;
		// (Posj - Posi) --> Resta de los vectores de posición
		dist = a.getPosition().distanceTo(b.getPosition());
		// fij = (G * mi * mj) / (dist)^2
		fij = (G_CTE *a.getMass()*b.getMass()) / Math.pow(dist , 2);
		//Fij = fij * Vector unitario de dij
		return (b.getPosition().minus(a.getPosition())).scale(1/dist).scale(fij);
	}
	
	@Override
	public String toString() {
		return "Newton´s law of universal gravitation (nlug)";
	}
	
}