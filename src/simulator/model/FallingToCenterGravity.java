package simulator.model;

import java.util.List;

public class FallingToCenterGravity implements GravityLaws {
	
	private static final double g = 9.81;

	@Override
	public void apply(List<Body> bodies) {
		
		for(Body b: bodies){
			/*
			 * El vector direccion lo obtenemos cambiando de sentido el vector posicion del cuerpo, ya que
			 * muestra la direccion desde el cuerpo b hasta el origen 0.
			 * Luego simplemente obtenemos el vector unitario y multiplicamos por F, que en este caso es la gravedad
			 */
			b.setAcceleration(b.getPosition().scale(-1).scale(1/b.getPosition().magnitude()).scale(g));
		}
	}
	
	@Override
	public String toString() {
		return "Falling to center gravity (ftcg)";
	}

}