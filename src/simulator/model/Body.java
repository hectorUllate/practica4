package simulator.model;

import simulator.misc.Vector;

public class Body {
	
	private String id;
	private Vector velocity;
	private Vector acceleration;
	private Vector position;
	private double mass;
	
	
	// Constructora
	public Body (String id, Vector position, Vector velocity, double mass){
		this.id = id;
		this.velocity = velocity;
		this.acceleration = velocity.scale(0); //Aceleracion inicial es 0
		this.position = position;
		this.mass = mass;
	}
	
	// Devuelve el identificador del cuerpo
	public String getId() {
		return this.id;
	}
	
	// Devuelve una copia del vector de velocidad
	public Vector getVelocity() {
		return this.velocity;
	}
	
	// Devuelve una copia del vector de aceleración
	public Vector getAcceleration() {
		return this.acceleration;
	}
	
	// Devuelve una copia del vector de posicion
	public Vector getPosition() {
		return this.position;
	}
	
	// Devuelve la masa del cuerpo
	public double getMass() {
		return this.mass;
	}
	
	// Hace una copia de v y se la asigna al vector de velocidad
	void setVelocity(Vector v) {
		this.velocity = v;
	}
	
	// Hace una copia de a y se la asigna al vector de aceleracion
	void setAcceleration(Vector a) {
		this.acceleration = a; 
	}
	
	// Hace una copia de p y se la asigna al vector de posicion
	void setPosition(Vector p) {
		this.position = p;
	}
	
	// Mueve el cuerpo durante t segundos utilizando los atributos del cuerpo mediante las fórmulas
	void move(double t) {
		this.setVelocity(this.getNewVelocity(t));	
		this.setPosition(this.getNewPosition(t));
	}
	
	@Override
	// Devuelve un string con la información del cuerpo en formato JSON
	public String toString() {
		return "{ \"id\": " + "\"" + this.id + "\"" + ", \"mass\": " + this.mass + ", \"pos\": " + this.position.toString() +
				", \"vel\": " + this.velocity.toString() +
				", \"acc\": " + this.acceleration.toString() + " }";
	}
	
	// Aplica la reduccion de masa a un cuerpo
	public void applyReduction(double lossFactor){
		this.mass *= (1 - lossFactor);
	}
	
	//Metodos privados
	private Vector getNewVelocity(double t){
		// v = v + a * t
		return this.velocity.plus(acceleration.scale(t));
	}
	
	private Vector getNewPosition(double t){
		// p = p + vt + 1/2 at^2
		return this.position.plus(this.velocity.scale(t).plus((this.acceleration.scale(Math.pow(t, 2)).scale(1/2))));
	}
	
}