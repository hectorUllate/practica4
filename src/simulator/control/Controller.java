package simulator.control;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import simulator.factories.Factory;
import simulator.model.Body;
import simulator.model.GravityLaws;
import simulator.model.PhysicsSimulator;
import simulator.model.SimulatorObserver;

public class Controller {

	private PhysicsSimulator physicsSimulator;
	private Factory<Body> factory;
	private Factory<GravityLaws> gLaws;
	private boolean _inFile;
	
	public Controller(PhysicsSimulator ps, Factory<Body> fb, Factory<GravityLaws> gLaws){
		this.physicsSimulator = ps;
		this.factory = fb;
		this.gLaws = gLaws;
		_inFile = false;
	}

	public boolean is_inFile() {
		return _inFile;
	}

	public void set_inFile(boolean _inFile) {
		this._inFile = _inFile;
	}

	public void loadBodies(InputStream in){
		
		JSONObject jsonInput = new JSONObject(new JSONTokener(in));
		JSONArray bodies = new JSONArray();
		
		// Tenemos en "bodies" todo el array de cuerpos como se muestra en "ex4.4.body.txt"
		bodies = jsonInput.getJSONArray("bodies");
		
		for (int i = 0; i < bodies.length(); i++) {
			this.physicsSimulator.addBody(this.factory.createInstance(bodies.getJSONObject(i)));
		}
	}
	
	public void run(int n, OutputStream out){
		
		PrintStream p = new PrintStream(out);
		p.println("{");
		p.println("\"states\": [");
		p.println(this.physicsSimulator.toString() + ",");
		for(int i = 0; i < n - 1; ++i) {
			this.physicsSimulator.advance();
			p.println(this.physicsSimulator.toString() + ",");
		}
		this.physicsSimulator.advance();
		p.println(this.physicsSimulator.toString() + "] }");
		p.close();
	}
	
	
	public void reset() {
		this.physicsSimulator.reset();
	}
	
	public void setDeltaTime(double dt) {
		this.physicsSimulator.setDeltaTime(dt);
	}
	
	public void addObserver(SimulatorObserver o) {
		this.physicsSimulator.addObserver(o);
	}
	
	public void run(int n) {
		for(int i = 0; i < n; ++i)
			this.physicsSimulator.advance();
	}
	
	public Factory<GravityLaws> getGravityLawsFactory() {
		return this.gLaws;
	}
	
	/* Usa la factoría de las leyes de gravedad (this.gLaws) actual para crear un nuevo objeto de tipo GravityLaws a partir de "info" y cambia
	 * las leyes de gravedad del simulador por él */
	public void setGravityLaws(JSONObject info) {
		this.physicsSimulator.setGravityLaws(this.gLaws.createInstance(info));
	}
	
}