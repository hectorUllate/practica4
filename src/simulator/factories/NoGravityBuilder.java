package simulator.factories;

import org.json.JSONObject;
import simulator.model.GravityLaws;
import simulator.model.NoGravity;

public class NoGravityBuilder extends Builder<GravityLaws> {
	
	public static final String type = "ng";
	public static final String desc = "Law of zero gravity";

	public NoGravityBuilder() {
		super(type, desc);
	}

	@Override
	public GravityLaws createTheInstance(JSONObject jo) {
		return new NoGravity();
	}

}