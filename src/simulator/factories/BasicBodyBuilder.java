package simulator.factories;

import org.json.JSONObject;

import simulator.misc.Vector;
import simulator.model.Body;

public class BasicBodyBuilder extends Builder<Body> {
	
	public static final String type = "basic";
	public static final String desc = "Default Body";
	
	public BasicBodyBuilder() {
		super(type, desc);
	}

	@Override
	public Body createTheInstance(JSONObject data) {
		
		if (data != null) {
			String id = data.getString("id");
			double[] pos = super.jsonArrayToDoubleArray(data.getJSONArray("pos"));
			double[] vel = super.jsonArrayToDoubleArray(data.getJSONArray("vel"));
			double mass = data.getDouble("mass");
			
			Body obj = new Body(id, new Vector(pos), new Vector(vel), mass);
			
			return obj;
		}
		throw new IllegalArgumentException("Hay un error en la seccion data");
	}
	
	public JSONObject createData() {
		
		JSONObject data = new JSONObject();
		data.put("id", "ID of the Basic Body");
		data.put("pos", "Initial position of the Basic Body");
		data.put("vel", "Initial velocity of the Basic Body");
		data.put("mass", "Initial mass of the Basic Body");
		return data;
	}

}