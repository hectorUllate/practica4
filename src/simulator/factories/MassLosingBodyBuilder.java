package simulator.factories;

import org.json.JSONObject;

import simulator.misc.Vector;
import simulator.model.Body;
import simulator.model.MassLosingBody;

public class MassLosingBodyBuilder extends Builder<Body>{

	public static final String type = "mlb";
	public static final String desc = "Mass Losing Body";
	
	public MassLosingBodyBuilder() {
		super(type, desc);
	}

	@Override
	public MassLosingBody createTheInstance(JSONObject data) {
		
		if (data != null) {
			String id = data.getString("id");
			double[] pos = super.jsonArrayToDoubleArray(data.getJSONArray("pos"));
			double[] vel = super.jsonArrayToDoubleArray(data.getJSONArray("vel"));
			double mass = data.getDouble("mass");
			double freq = data.getDouble("freq");
			double factor = data.getDouble("factor");
			MassLosingBody obj = new MassLosingBody(id, new Vector(pos), new Vector(vel), mass, freq, factor);
			
			return obj;
		}
		throw new IllegalArgumentException("Hay un error en la seccion data");
	}
	
	public JSONObject createData() {
		
		JSONObject data = new JSONObject();
		data.put("id", "ID of the Basic Body");
		data.put("pos", "Initial position of the Basic Body");
		data.put("vel", "Initial velocity of the Basic Body");
		data.put("mass", "Initial mass of the Basic Body");
		data.put("freq", "Frequency in which the Basic Body losses mass");
		data.put("factor", "Factor in which the Basic Body losses mass");
		return data;
		
	}

}