package simulator.factories;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class BuilderBasedFactory<T> implements Factory<T> {
	
	List<Builder<T>> bList;
	
	public BuilderBasedFactory(List<Builder<T>> builders) {
		this.bList = builders;
	}

	@Override
	public T createInstance(JSONObject info) {
		T r;
		for(Builder<T> b : bList) {
			r = b.createInstance(info);
			if(r != null) return r;
		}
		throw new IllegalArgumentException("No se ha podido crear el objeto correspondiente.");
	}

	@Override
	public List<JSONObject> getInfo() {
		ArrayList<JSONObject> infoList = new ArrayList<>();
		for(Builder<T> b : bList) {
			infoList.add(b.getBuilderInfo());
		}
		return infoList;
	}

}