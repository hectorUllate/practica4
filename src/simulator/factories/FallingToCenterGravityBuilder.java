package simulator.factories;

import org.json.JSONObject;
import simulator.model.FallingToCenterGravity;
import simulator.model.GravityLaws;

public class FallingToCenterGravityBuilder extends Builder<GravityLaws> {
	
	public static final String type = "ftcg";
	public static final String desc = "Falling to center gravity law";

	public FallingToCenterGravityBuilder() {
		super(type, desc);
	}

	@Override
	public GravityLaws createTheInstance(JSONObject jo) {
		return new FallingToCenterGravity();
	}

}