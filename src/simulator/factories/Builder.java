package simulator.factories;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class Builder<T> {
	
	String type;
	String desc;
	
	public Builder(String t, String d) {
		this.type = t;
		this.desc = d;
	}
	
	protected double[] jsonArrayToDoubleArray(JSONArray ja) {
		double[] array = new double[ja.length()];
		for (int i = 0; i < ja.length(); i++) {
			array[i] = ja.getDouble(i);
		}
		return array;
	}
	
	public T createInstance(JSONObject info) {
		T obj = null;
		
		if(this.type != null && this.type.equals(info.getString("type"))) 
			obj = createTheInstance(info.getJSONObject("data"));
		
		return obj;
	}
	
	public JSONObject getBuilderInfo() {
		JSONObject jo = new JSONObject();
		
		jo.put("type", this.type);
		
		jo.put("data", createData());
		
		jo.put("desc", this.desc);
		
		return jo;
	}
	
	public JSONObject createData() {
		JSONObject data = new JSONObject();
		return data;
	}
	
	public abstract T createTheInstance(JSONObject jo);

}