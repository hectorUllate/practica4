package simulator.factories;

import org.json.JSONObject;
import simulator.model.GravityLaws;
import simulator.model.NewtonUniversalGravitation;

public class NewtonUniversalGravitationBuilder extends Builder<GravityLaws>{

	public static final String type = "nlug";
	public static final String desc = "Newton's law of universal gravitation";
	
	public NewtonUniversalGravitationBuilder() {
		super(type,desc);
	}

	@Override
	public GravityLaws createTheInstance(JSONObject jo) {
		// Devolvemos directamente la ley correspondiente porque no tiene data y si estamos en este método es porqe ya ha comprobado la key "type"
		return new NewtonUniversalGravitation();
	}
	
	// El createData() no es necesario implementarlo porque por herencia se ejecutará el método de la clase padre

}